# TrueCaptcha Bot
This telegram bot will help you to protect chat from automated bots. It much more better than t.me/Cyberdyne_Systems_bot, because it check that user can recognize image and choose correct emoji (or guess it with small chance).

Features of this bot:
 * mutes messages from new users and wait 5 min for solved captcha, than ban user for a day and deletes message
 * greeting user that correctly solved captcha
 * you can load new emojis and images in bot without any reloads
 * if it crashed it remembers correct answers to pending captchas and can check users if image/emoji deleted

You can use this bot in Telegram by [this nick](https://t.me/truecaptcha_bot) or install it on your own bot-account!


## Usage
To use this bot you must make him administrator and give restrict users permission

## Install your own bot
Firstly, clone this repo localy: `git clone https://gitlab.com/woem/truecaptcha_bot.git && cd truecaptcha_bot`
Then install libraries: `pip install -r requirements.txt`
You can use systemd for managing bot startup with my [sample systemd unit file](https://gitlab.com/woem/truecaptcha_bot/blob/master/truecaptcha_bot.service). To use it, you must change `11111111:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA` to your bot's token, and `/path/to/bot/folder` to folder with this bot. Than you should run `systemctl daemon-reload && systemctl start truecaptcha_bot && systemctl enable truecaptcha_bot` and your bot will run even after server reboots.

### Adding more emojis and images
All possible emojis and images stored in `/path/to/bot/folder/captcha/`.

To add emoji, simple create new folder in directory `/path/to/bot/folder/captcha/` with emoji symbol.
_Example_: to add emoji of open book (`📖`) you must create folder `/path/to/bot/folder/captcha/📖/`

To add image, choose which emoji corresponds this image and save picture in folder `/path/to/bot/folder/captcha/_emoji_`. For the convenience of the user, use one image to only one emoji.
_Example_: to add img_1.jpg corresponding emoji of open book (`📖`) you must put file to `/path/to/bot/folder/captcha/📖/img_1.jpg`

### Security advice
You should add more emojis (and images to them) for gathering higher protection from bots (more possible options cause less guessing chance to bot)
